package usp.ime.vision.mobileocr;

import java.io.InputStream;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import usp.ime.vision.opencv.OcrPreProcess;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;


public class ProcessImageTask extends AsyncTask<String, Integer, Bitmap>{

	private Bitmap image;
	private String fileName;
	
	public ProcessImageTask(Bitmap image)
	{	
		this.image=image;
	}

	@Override
	protected Bitmap doInBackground(String... params) {
		InputStream inputStream;
		try {
			//#TODO: Quando compilar direito as bibliotecas do OpenCV descomentar aqui
	        Mat inputMat=new Mat();
	        
	        Utils.bitmapToMat(image, inputMat);
	        Mat outputMat=new Mat(inputMat.rows(),inputMat.cols(), inputMat.type());
	        
	        OcrPreProcess preProcess=OcrPreProcess.getInstance();
	        preProcess.preProcess(inputMat, outputMat);
	        Bitmap processedPicture=Bitmap.createBitmap(outputMat.rows(), outputMat.cols(), Bitmap.Config.ARGB_8888);
	        
	        
	        OcrPipeline ocrPipeline=OcrPipeline.getInstance();
	        
	        try {
	        	ocrPipeline.clear();
	        	ocrPipeline.setPageSegMode(OcrPipeline.PageSegMode.PSM_AUTO);
	        	ocrPipeline.setImage(processedPicture);
	        	String ocr=ocrPipeline.getUTF8Text();
	        	
	        	
	        	//#TODO: Falta matar tesseract quando é destruida a classe
	        	//ocrPipeline.end();
	        	image.recycle();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
			return null;
		} catch (Exception e) {
			return null;
		} 	
		
	}

	

}