package usp.ime.vision;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import usp.ime.vision.mobileocr.OcrPipeline;
import usp.ime.vision.opencv.OcrPreProcess;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.YuvImage;
import android.os.Build;

public class OcrActivity extends Activity {

	
	private ImageView imageView;
	private String fileName;
	private int height;
	private int width;
	private Context ctx;
	
	class ProcessImageTask extends AsyncTask<String, Integer, Bitmap>{
		
		private Bitmap processedPicture;
		private String ocr;
		
		public ProcessImageTask()
		{	
		}
		

		@Override
		protected Bitmap doInBackground(String... params) {
			
			try {
				//FileInputStream input=ctx.openFileInput(fileName);
				
				//#TODO: Quando compilar direito as bibliotecas do OpenCV descomentar aqui
		        Mat inputMat=new Mat();		        
		        File imageFile = new File(fileName);
		        FileInputStream input=new FileInputStream(imageFile);
		        Bitmap image=BitmapFactory.decodeStream(input);
		        Utils.bitmapToMat(image, inputMat);
		        
		        Mat outputMat=new Mat();
		        
		        OcrPreProcess preProcess=OcrPreProcess.getInstance();
		        preProcess.preProcess(inputMat, outputMat);
		        Mat tmp = new Mat (outputMat.rows(), outputMat.cols(), CvType.CV_16U);
		        try {
		            //Imgproc.cvtColor(seedsImage, tmp, Imgproc.COLOR_RGB2BGRA);
		        	try{
		        		Mat tmp2 = new Mat (outputMat.rows(), outputMat.cols(), CvType.CV_8UC4);
		        		Imgproc.cvtColor(outputMat, tmp2, Imgproc.COLOR_GRAY2BGRA);
		        		processedPicture = Bitmap.createBitmap(tmp2.cols(), tmp2.rows(), Bitmap.Config.ARGB_8888);
			            Utils.matToBitmap(tmp2, processedPicture, true);
		        	}catch(Exception e)
		        	{
		        		Log.d("Exception",e.getMessage());
		        	}
		        	
//		        	try{
//		        		Mat tmp3 = new Mat (outputMat.rows(), outputMat.cols(), CvType.CV_16UC1);
//		        		Imgproc.cvtColor(outputMat, tmp3, Imgproc.COLOR_GRAY2RGB);
//		        	}catch(Exception e)
//		        	{
//		        		Log.d("Exception",e.getMessage());
//		        	}
//		        	
//		        	try{
//		        		Mat tmp3 = new Mat (outputMat.rows(), outputMat.cols(), CvType.CV_16UC1);
//		        		Imgproc.cvtColor(outputMat, tmp3, Imgproc.COLOR_GRAY2RGB);
//		        	}catch(Exception e)
//		        	{
//		        		Log.d("Exception",e.getMessage());
//		        	}
//		        	
//		        	try{
//		        		Mat tmp3 = new Mat (outputMat.rows(), outputMat.cols(), CvType.CV_16UC1);
//		        		Imgproc.cvtColor(outputMat, tmp3, Imgproc.COLOR_GRAY2RGB);
//		        	}catch(Exception e)
//		        	{
//		        		Log.d("Exception",e.getMessage());
//		        	}
		            
		        }
		        catch (CvException e){Log.d("Exception",e.getMessage());}
		        //processedPicture=image;
//		        processedPicture=Bitmap.createBitmap(outputMat.rows(), outputMat.cols(), Bitmap.Config.RGB_565);
//		        Utils.matToBitmap(outputMat, processedPicture);
				
		        
		        OcrPipeline ocrPipeline=OcrPipeline.getInstance();
//		        
		        try {
		        	ocrPipeline.clear();
		        	ocrPipeline.setPageSegMode(OcrPipeline.PageSegMode.PSM_AUTO);
		        	ocrPipeline.setImage(processedPicture);
		        	ocr=ocrPipeline.getUTF8Text();
		        	
		        	
		        	//#TODO: Falta matar tesseract quando é destruida a classe
		        	//ocrPipeline.end();
		        	/*image.recycle();*/
		        } catch (Exception e) {
		            throw new RuntimeException(e);
		        }
				return null;
			} catch (Exception e) {
				throw new RuntimeException(e);
			} 	
			
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			imageView.setImageBitmap(processedPicture);
			Toast.makeText(ctx, ocr, Toast.LENGTH_LONG).show();
			/*processedPicture.recycle();*/
		}
		
		
		
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ocr);
		imageView=(ImageView) findViewById(R.id.imageView);
		
		fileName=getIntent().getExtras().getString("imageFilename");
		width=getIntent().getExtras().getInt("width");
		height=getIntent().getExtras().getInt("height");
		ctx=this;
		// Show the Up button in the action bar.
		setupActionBar();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ocr, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		ProcessImageTask imageTask=new ProcessImageTask();
		imageTask.execute("");
		super.onPostCreate(savedInstanceState);
	}

}
