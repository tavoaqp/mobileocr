package usp.ime.vision.opencv;

import org.opencv.core.Mat;

public class OcrPreProcess {
	
	private static OcrPreProcess singleton;
	
	private OcrPreProcess(){
		
	}
	
	public static OcrPreProcess getInstance()
	{
		if (singleton==null)
			singleton=new OcrPreProcess();
		return singleton;
	}
	
	public static void loadJNI() {
		System.loadLibrary("pre_mobileocr");
	}

	public void preProcess (Mat input, Mat output)
	{		
		nativePreProcess(input.getNativeObjAddr(), output.getNativeObjAddr());
	}
	
	private native void nativePreProcess(long input, long output);
}
