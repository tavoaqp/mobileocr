package usp.ime.vision.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.opencv.android.JavaCameraView;
import org.opencv.android.Utils;
import org.opencv.core.Mat;

import usp.ime.vision.OcrActivity;
import usp.ime.vision.R;
import usp.ime.vision.mobileocr.OcrPipeline;
import usp.ime.vision.opencv.OcrPreProcess;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Toast;

public class MobileOCRView extends JavaCameraView implements PictureCallback {

	private static final String TAG = "IMEVision::MobileOCR";
	private OcrPipeline ocrPipeline;
	private OcrPreProcess preProcess;
	private Bitmap picture;

	private String text;
	private String mPictureFileName;

	public MobileOCRView(Context context, AttributeSet attrs) {
		super(context, attrs);

	}

	public List<String> getEffectList() {
		return mCamera.getParameters().getSupportedColorEffects();
	}

	public boolean isEffectSupported() {
		return (mCamera.getParameters().getColorEffect() != null);
	}

	public String getEffect() {
		return mCamera.getParameters().getColorEffect();
	}

	public void setEffect(String effect) {
		Camera.Parameters params = mCamera.getParameters();
		params.setColorEffect(effect);
		mCamera.setParameters(params);
	}

	public List<Size> getResolutionList() {
		return mCamera.getParameters().getSupportedPreviewSizes();
	}

	public void setResolution(Size resolution) {
		disconnectCamera();
		mMaxHeight = resolution.height;
		mMaxWidth = resolution.width;
		connectCamera(getWidth(), getHeight());
	}

	public Size getResolution() {
		return mCamera.getParameters().getPreviewSize();
	}
	
	/**
	 * Converts YUV420 NV21 to RGB8888
	 * 
	 * @param data byte array on YUV420 NV21 format.
	 * @param width pixels width
	 * @param height pixels height
	 * @return a RGB8888 pixels int array. Where each int is a pixels ARGB. 
	 */
	public int[] convertYUV420_NV21toRGB8888(byte [] data, int width, int height) {
	    int size = width*height;
	    int offset = size;
	    int[] pixels = new int[size];
	    int u, v, y1, y2, y3, y4;

	    // i percorre os Y and the final pixels
	    // k percorre os pixles U e V
	    for(int i=0, k=0; i < size; i+=2, k+=2) {
	        y1 = data[i  ]&0xff;
	        y2 = data[i+1]&0xff;
	        y3 = data[width+i  ]&0xff;
	        y4 = data[width+i+1]&0xff;

	        u = data[offset+k  ]&0xff;
	        v = data[offset+k+1]&0xff;
	        u = u-128;
	        v = v-128;

	        pixels[i  ] = convertYUVtoRGB(y1, u, v);
	        pixels[i+1] = convertYUVtoRGB(y2, u, v);
	        pixels[width+i  ] = convertYUVtoRGB(y3, u, v);
	        pixels[width+i+1] = convertYUVtoRGB(y4, u, v);

	        if (i!=0 && (i+2)%width==0)
	            i+=width;
	    }

	    return pixels;
	}

	private  int convertYUVtoRGB(int y, int u, int v) {
	    int r,g,b;

	    r = y + (int)1.402f*v;
	    g = y - (int)(0.344f*u +0.714f*v);
	    b = y + (int)1.772f*u;
	    r = r>255? 255 : r<0 ? 0 : r;
	    g = g>255? 255 : g<0 ? 0 : g;
	    b = b>255? 255 : b<0 ? 0 : b;
	    return 0xff000000 | (b<<16) | (g<<8) | r;
	}

	public void takePicture(final String fileName) {
		Log.i(TAG, "Tacking picture");
		mPictureFileName = fileName;

		final Context context = getContext();
		PreviewCallback callb=new PreviewCallback(){

			@Override
			public void onPreviewFrame(byte[] data, Camera camera) {
				
				try {
					Camera.Parameters parameters = camera.getParameters();
					Size size = parameters.getPreviewSize();
					
					
			        
			        
			        YuvImage image = new YuvImage(data, ImageFormat.NV21,
			                size.width, size.height, null);
			        
			        File file = new File(mPictureFileName);
			        FileOutputStream filecon = new FileOutputStream(file);
			        image.compressToJpeg(
			                new Rect(0, 0, image.getWidth(), image.getHeight()), 90,
			                filecon);
			        
			        
			        int pixels[]=convertYUV420_NV21toRGB8888(data, size.width, size.height);
			        filecon.close();
			        Intent intent = new Intent(context, OcrActivity.class);
					intent.putExtra("imageFilename", mPictureFileName);
					intent.putExtra("width", image.getWidth());
					intent.putExtra("height", image.getHeight());
					getContext().startActivity(intent);
			    } catch (Exception e) {
			        Toast toast = Toast
			                .makeText(context, e.getMessage(), 1000);
			        toast.show();
			    }
			
			}
			
		};
		mCamera.setOneShotPreviewCallback(callb);
		PictureCallback callback = new PictureCallback() {

			@Override
			public void onPictureTaken(byte[] data, Camera camera) {
//				Log.i(TAG, "Saving a bitmap to file");
//				Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
//				Context ctx = getContext();				
//
//				FileOutputStream out;
//				try {
//					 out = new FileOutputStream(mPictureFileName);
//					//out = ctx.openFileOutput(mPictureFileName, Context.MODE_PRIVATE);
//					picture.compress(Bitmap.CompressFormat.JPEG, 90, out);
//					picture.recycle();
//					out.close();
//					// mCamera.startPreview();
//					// mCamera.stopPreview();
//					// mCamera.release();
//					Log.i(TAG, "Calling another activity");
//
//					Intent intent = new Intent(context, OcrActivity.class);
//					intent.putExtra("imageFilename", mPictureFileName);
//					getContext().startActivity(intent);
//				} catch (Exception e) {
//					throw new RuntimeException(e);
//				}
			}
		};
		//System.gc();
		//mCamera.takePicture(null, callback, callback);

	}

	public void showRecognizedText(String text) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
		builder.setMessage(text).setTitle("Resultado OCR").create().show();

	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public void onPictureTaken(byte[] data, Camera camera) {
		Log.i(TAG, "Saving a bitmap to file");
		Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
		Context ctx = getContext();

		FileOutputStream out;
		try {
			// out = new FileOutputStream(mPictureFileName);
			out = ctx.openFileOutput(mPictureFileName, Context.MODE_PRIVATE);
			picture.compress(Bitmap.CompressFormat.JPEG, 90, out);
			picture.recycle();
			out.close();
			// mCamera.startPreview();
			// mCamera.stopPreview();
			// mCamera.release();
			Log.i(TAG, "Calling another activity");

			Intent intent = new Intent(this.getContext(), OcrActivity.class);
			intent.putExtra("imageFilename", mPictureFileName);
			getContext().startActivity(intent);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

}
