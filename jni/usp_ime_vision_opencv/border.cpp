#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>

using namespace cv;

/// Global Variables
Mat src;
int top, bottom, left, right;
int borderType;
Scalar value;
char* window_name = "copyMakeBorder Demo";
RNG rng(12345);

/** @function main  */
int main( int argc, char** argv )
{

  int c;

  /// Load an image
  src = imread( argv[1] );

  if( !src.data )
  { return -1;
    printf(" No data entered, please enter the path to an image file \n");
  }

  /// Brief how-to for this program
  printf( "\n \t copyMakeBorder Demo: \n" );
  printf( "\t -------------------- \n" );
  printf( " ** Press 'c' to set the border to a random constant value \n");
  printf( " ** Press 'r' to set the border to be replicated \n");
  printf( " ** Press 'ESC' to exit the program \n");

  /// Create window
  namedWindow( window_name, CV_WINDOW_AUTOSIZE );

  /// Initialize arguments for the filter
  top = 10; bottom = 10;
  left = 10; right = 10;
  Mat dst=Mat(src.rows-20, src.cols-20, src.type());
  printf("rows= %i , cols = %i \n", src.rows, src.cols);  
printf("rows= %i , cols = %i \n", dst.rows, dst.cols);  
  imshow( window_name, src );
  waitKey(5000);
  value = Scalar( 255, 255, 255 );
  copyMakeBorder( dst, dst, top, bottom, left, right, BORDER_CONSTANT, value );

  printf("rows= %i , cols = %i \n", dst.rows, dst.cols);

  imshow( window_name, dst );

  waitKey(5000);
  return 0;
}