
import os
import sys
import numpy
import Image
import cv2
from matplotlib import pyplot as plt
from pylab import *
from mpl_toolkits.mplot3d import Axes3D

def gaborconvolve(im,nscale,norient,minWaveLength,mult,sigmaOnf):
    
    (cols,rows) = im.size

    if cols%2 != 0:
        cols = cols - 1
    if rows%2 != 0:
        rows = rows - 1

    # resize image to reduce calculate time.
    cols = cols/2
    rows = rows/2
    im = im.resize((cols,rows),Image.ANTIALIAS)

    imagefft = numpy.fft.fft2(im)

    x_range = [ x/float(cols) for x in range(-cols/2,cols/2) ]
    y_range = [ y/float(rows) for y in range(-rows/2,rows/2) ]
    x,y = numpy.meshgrid(x_range,y_range)

    radius = numpy.sqrt(x*x+y*y)
    theta = numpy.arctan2(y,x)

    radius = numpy.fft.ifftshift(radius)
    radius[0][0] = 1.0
    theta = numpy.fft.ifftshift(theta)

    sintheta = numpy.sin(theta)
    costheta = numpy.cos(theta)

    # low pass filter 
    lp = numpy.fft.ifftshift( 1.0/(numpy.power((numpy.sqrt(x*x+y*y) / 0.45),(2*15)) + 1.0) )

    BP = []
    logGabor = []

    for s in range(0,nscale):
        wavelength = minWaveLength*mult**s
        fo = 1.0 / wavelength
        l = numpy.exp( ( -(numpy.log(radius/fo))**2 )  /   (2.0*numpy.log(sigmaOnf)**2)  )
        l = l*lp
        l[0][0] = 0.0
    
        logGabor.append(l)
        # TODO/FIXME
        BP.append(numpy.fft.ifft2(imagefft*l)) 

    logGabor = numpy.array(logGabor)
    BP = numpy.array(BP)

    EO = []

    for o in range(0,norient):
        angl = o*numpy.pi/norient
        wavelength = minWaveLength

        ds = sintheta * numpy.cos(angl) - costheta * numpy.sin(angl)
        dc = costheta * numpy.cos(angl) + sintheta * numpy.sin(angl)
        dtheta = numpy.abs(numpy.arctan2(ds,dc))
        dtheta = numpy.minimum(dtheta*norient/2.0,numpy.pi)

        spread = (numpy.cos(dtheta)+1.0)/2.0

        tmpEO = []
        for s in range(0,nscale):
            Filter = logGabor[s]*spread

            # TODO/FIXME
            result = numpy.fft.ifft2(imagefft*Filter)
            tmpEO.append(result)

        EO.append(numpy.array(tmpEO))

    EO = numpy.array(EO)

    # EO[orient][scale]
    return EO



img=cv2.imread("13_test.jpg",0)
imagefft = numpy.fft.fft2(img)
plt.imshow(img)
plt.show()

(cols,rows) = img.shape
#print img.shape
x_range = [ x/float(cols) for x in range(-cols/2,cols/2) ]
y_range = [ y/float(rows) for y in range(-rows/2,rows/2) ]
#print "x_range=\n",x_range,"\n"
x,y = numpy.meshgrid(x_range,y_range)
#print "GRID=\n",x,"\n=========================",y

radius = numpy.sqrt(x*x+y*y)
#print "RADIUS=\n",radius
theta = numpy.arctan2(y,x)
radius[rows/2][cols/2]=1

sintheta = numpy.sin(theta)
costheta = numpy.cos(theta)


BP = []
logGabor = []
nscale=10
minWaveLength=3
mult=2
sigmaOnf=10
dThetaOnSigma=0.2

lp = numpy.fft.ifftshift( 1.0/(numpy.power((numpy.sqrt(x*x+y*y) / 0.45),(2*15)) + 1.0) )

for s in range(0,nscale):
    wavelength = minWaveLength*mult**s
    fo = 1.0 / wavelength
    l = numpy.exp( ( -(numpy.log(radius/fo))**2 )  /   (2.0*numpy.log(sigmaOnf)**2)  )   
    #l=numpy.multiply(l,lp)
    l[rows/2][cols/2] = 0.0    
    #l[1][1] = 0.0    
    # L=numpy.sqrt(numpy.sum(numpy.power(l,2)))
    # l=l/L
    logGabor.append(l) 
    

logGabor = numpy.array(logGabor)
norient=10
filters=[]

for o in range(0,norient):
    angl = o*numpy.pi/norient
    wavelength = minWaveLength

    ds = sintheta * numpy.cos(angl) - costheta * numpy.sin(angl)
    dc = costheta * numpy.cos(angl) + sintheta * numpy.sin(angl)
    dtheta = numpy.abs(numpy.arctan2(ds,dc))    
    #dtheta = numpy.minimum(dtheta*norient/2.0,numpy.pi)    
    dtheta = numpy.minimum(dtheta,numpy.pi)    

    thetaSigma = numpy.pi/norient/dThetaOnSigma;  
    spread = numpy.exp(-(dtheta**2) / (2 * thetaSigma**2));  
    #spread = (numpy.cos(dtheta)+1.0)/2.0

    #print "spread=",spread
    
    for s in range(0,nscale):
        Filter = logGabor[s]*spread
        L = sqrt(sum(numpy.power(numpy.real(Filter),2) + numpy.power(numpy.imag(Filter),2) ))/sqrt(2);
        Filter=Filter/L
        #print logGabor[s].shape
        #print spread.shape
        # filters.append(Filter)
        #plt.imshow(Filter);
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        surf=ax.plot_surface(x,y,Filter,cmap=cm.jet,
        linewidth=0, antialiased=True)

        # ax.set_zlim(-1.01, 1.01)
        # ax.zaxis.set_major_locator(LinearLocator(10))
        # ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

        plt.show()    

#for gabfil in filters:

    #print gabfil
    #plt.imshow(cv2.filter2D(img,-1,gabfil))
    