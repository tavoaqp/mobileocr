#include "opencv2/opencv.hpp"
#include <stdio.h>

using namespace cv;

void edgeDetection(Mat &input, Mat &output);
Mat mkKernel(int ks, double sig, double th, double lm, double ps);

int kernel_size=21;
int pos_sigma= 5;
int pos_lm = 50;
int pos_th = 0;
int pos_psi = 90;
Mat src_f;
Mat dest;
Mat output;

void Process(int , void *)
{
    double sig = pos_sigma;
    double lm = 0.5+pos_lm/100.0;
    double th = pos_th;
    double ps = pos_psi;
    Mat kernel = mkKernel(kernel_size, sig, th, lm, ps);
    filter2D(output, dest, CV_32F, kernel);
    imshow("Process window", dest);
    Mat Lkernel(kernel_size*20, kernel_size*20, CV_32F);
    resize(kernel, Lkernel, Lkernel.size());
    Lkernel /= 2.;
    Lkernel += 0.5;
    imshow("Kernel", Lkernel);
    Mat mag;
    pow(dest, 2.0, mag);
    imshow("Mag", mag);
}

int main(int argc, char *argv[]) {
    Mat frame;
    VideoCapture cap;
    if (argc > 1) {
        cap = VideoCapture(argv[1]);
    } else {
        cap = VideoCapture(0);
    }
    
    if (!cap.isOpened()) {
        return -1;
    }
    
    cap >> frame;    
    
    //imshow("Src", frame);
    edgeDetection(frame,output);
    imshow("Output", output);
    if (!kernel_size%2)
    {
        kernel_size+=1;
    }
    namedWindow("Process window", 2);
    createTrackbar("Sigma", "Process window", &pos_sigma, kernel_size, Process);
    createTrackbar("Lambda", "Process window", &pos_lm, 100, Process);
    createTrackbar("Theta", "Process window", &pos_th, 180, Process);
    createTrackbar("Psi", "Process window", &pos_psi, 360, Process);
    Process(0,0);
    waitKey(0);

    /*imshow("Video", frame);
    
    imshow("Video1", output);
    while(waitKey(5000)!=27);*/
    
    
    return 0;
}

/*int main(int argc, char** argv)
{    
    VideoCapture cap;
    if (argc > 1) {
        cap = VideoCapture(argv[1]);
    } else {
        cap = VideoCapture(0);
    }

    if (!cap.isOpened()) {
        return -1;
    }
    Mat image;
    cap >> image;
    
    imshow("Src", image);
    Mat src;
    cvtColor(image, src, CV_BGR2GRAY);
    src.convertTo(src_f, CV_32F, 1.0/255, 0);
    if (!kernel_size%2)
    {
        kernel_size+=1;
    }
    namedWindow("Process window", 1);
    createTrackbar("Sigma", "Process window", &pos_sigma, kernel_size, Process);
    createTrackbar("Lambda", "Process window", &pos_lm, 100, Process);
    createTrackbar("Theta", "Process window", &pos_th, 180, Process);
    createTrackbar("Psi", "Process window", &pos_psi, 360, Process);
    Process(0,0);
    waitKey(0);
    return 0;
}
*/


