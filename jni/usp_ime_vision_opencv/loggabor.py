    def loggabor(size, theta, f, sigma):
        h, w = size
        x, y = iameshgrid(arange(-w/2,w/2), arange(-h/2,h/2))
        
        xt = x*cos(theta)+y*sin(theta)
        yt = y*cos(theta)-x*sin(theta)
        
        return exp(-0.5*((xt/sigma[0])**2+(yt/sigma[1])**2))*cos(2*pi*f*xt)