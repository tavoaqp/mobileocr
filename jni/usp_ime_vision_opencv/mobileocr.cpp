#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <math.h>
#include <stdio.h>
#include <iostream>

using namespace cv;
using namespace std;

void edgeDetection(Mat &input, Mat &output);

Mat src_f;

int main(int argc, char** argv)
{    
    Mat image=imread(argv[1],1);

    Mat output;
    
    
    imshow("Source", image); 
    edgeDetection(image,src_f);
    imshow("Output", src_f);
    
    waitKey(0);
    return 0;
}
