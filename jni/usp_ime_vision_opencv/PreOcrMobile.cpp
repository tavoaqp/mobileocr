	#include "opencv2/opencv.hpp"
	#include "opencv2/imgproc/imgproc.hpp"
	#include <stdio.h>
	#include <iostream>
	#include <math.h>
	#include <stdlib.h>
	#include <limits>
	#include <algorithm>
	/*#include <android/log.h>*/
/*
	#define  LOG_TAG    "mobileOCR"
	#define  ALOG(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)*/

	using namespace std;
	using namespace cv;

	void openClose(Mat &input, Mat &output, Mat &element);
	void closeOpen(Mat &input, Mat &output, Mat &element);
	void opening(Mat &input, Mat& output, Mat &element);
	void closing(Mat &input, Mat& output, Mat &element);
	void gradient(Mat &input, Mat &output, Mat &element);
	void applyFilters(Mat &input, Mat &output);
	void applyThreshold(Mat &input, Mat &output, double threshold);
	void componentLabeling(Mat &input, Vector<Point> groups);
	void closeholes(Mat &input, Mat &element);
	void infreconstruction(Mat &input, Mat &cond, Mat &element, Mat &output);
	void conditional_dilate(Mat &input, Mat &cond, Mat &element, int n, Mat &output);

	void getLogGaborKernel(Mat &kernel,int cols, int rows, double minWaveLength, double sigmaOnf, double thetaZero);
	void reconstructColorFromMask(Mat &input, Mat &output, Mat &mask);
	void meshgrid(const Range &xgv, const Range &ygv, Mat &X, Mat &Y);
	double customKmeans( InputArray _data, int K,
	                   InputOutputArray _bestLabels,
	                   TermCriteria criteria, int attempts,
	                   int flags, OutputArray _centers );
	void runKmeansOnImage(Mat &input, int numClusters, Mat clusters[]);
	double calcRMeasure(vector<vector<Point> > &contours);
	void drawContours(vector<vector<Point> > &contours, vector<Vec4i> hierarchy, Size size, Mat &drawing);
	void skeleton(Mat &input, Mat& output);

	bool sortlevel(pair<int, double> a, pair<int, double> b){
    	return a.second < b.second;
  }

  bool sortSegment(pair<Mat, double> a, pair<Mat, double> b){
    	return a.second > b.second;
  }

void centerOfMass(Point &point, Point &center)
{
	center.x+=point.x;
	center.y+=point.y;
}



double morphoThreshold(Mat &gray, Mat &output)
{
	Mat element = getStructuringElement( MORPH_RECT,
	                                       Size( 3, 3 ));

	    Mat openclose_result;
		openClose(gray,openclose_result, element);

		Mat closeopen_result;
		closeOpen(gray,closeopen_result, element);

		Mat sum_result=openclose_result+closeopen_result;
		
		openclose_result.release();
		closeopen_result.release();

		double weight=2;
		Mat divide_result=sum_result/weight;

		sum_result.release();

		Mat gradient_result;
		gradient(divide_result, gradient_result, element);	

		divide_result.release();

		Mat filter_result;
		applyFilters(gradient_result, filter_result);

		double threshold_num=sum(gradient_result.mul(filter_result))[0];
		double threshold_den=sum(filter_result)[0];
		double threshold=threshold_num/threshold_den;

		filter_result.release();

		Mat threshold_result;	
		applyThreshold(gradient_result, threshold_result, threshold);	
		output=threshold_result;
		return threshold;
}

void findFinalCluster(Mat &input, int numClusters, Mat *clusters, Mat &finalCluster)
{
	vector<pair<int,double> > r_measures;		

		for (int idx=0;idx<numClusters;idx++)
		{
			vector<vector<Point> > contours;
			vector<Vec4i> hierarchy;
			findContours( clusters[idx].clone(), contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
			
			r_measures.push_back(make_pair(idx,calcRMeasure(contours)));			
		}		

		std::sort (r_measures.begin(), r_measures.begin()+numClusters,sortlevel); 
		

		vector<vector<Point> > mergeContours;
		vector<Vec4i> hierarchy;
		Mat mergeCluster=Mat::zeros(input.size(),CV_8U);
		bitwise_or(clusters[r_measures[0].first], clusters[r_measures[2].first], mergeCluster);
		
		findContours( mergeCluster.clone(), mergeContours,hierarchy ,CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

		double merge_rmeasure=calcRMeasure(mergeContours);

		if (merge_rmeasure>r_measures[0].second)
		{
			bitwise_not(clusters[r_measures[0].first],finalCluster);
		}
		else
		{
			bitwise_not(mergeCluster,finalCluster);
			//finalCluster=mergeCluster;
		}

}

void segmentLetters(Mat &input, Mat &output)
{
	vector<vector<Point> > resContours;
		vector<Vec4i> resHierarchy;		
		findContours( input.clone(), resContours, resHierarchy ,CV_RETR_EXTERNAL, CV_CHAIN_APPROX_TC89_L1, Point(0, 0) );		 		
  		
  		vector<Point2f> massCenters( resContours.size() );
  		for( int i = 0; i < resContours.size(); i++ )
     		{ 
     			double x=0.0,y=0.0;     			
     			
     			for (int j=0; j<resContours[i].size();j++)
     			{
     				x+=resContours[i][j].x;
     				y+=resContours[i][j].y;
     			}
     			x/=resContours[i].size();
     			y/=resContours[i].size();
     			massCenters[i] = Point2f(x ,y ); 
     		}

		Mat features=Mat(massCenters.size(), 3, CV_32F);
		for( int idx=0;idx<massCenters.size();idx++)
			{
				features.at<float>(idx,0)=massCenters[idx].x;
				features.at<float>(idx,1)=massCenters[idx].y;
				
				vector<Point> poly;
				approxPolyDP( Mat(resContours[idx]), poly, 3, true );

				Rect boundRect;
       			boundRect = boundingRect( Mat(poly) );

       			features.at<float>(idx,2)=boundRect.height*boundRect.width;

			}		    
		
		Mat bestLabels;
		TermCriteria criteria=TermCriteria(TermCriteria::MAX_ITER & TermCriteria::EPS, 30, 0.2 );
		int numMassClusters=30;
		if (numMassClusters>massCenters.size())
		{
			numMassClusters=massCenters.size()/2;
		}

		cv::kmeans(features, numMassClusters, bestLabels, criteria,1, KMEANS_PP_CENTERS);	

		RNG rng(12345);

		vector<Scalar> colors;
		for (int i=0;i<numMassClusters;i++)
		{
			Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
			colors.push_back(color);
		}

		Scalar white=Scalar(255,255,255);
		vector<pair<Mat, double> > segments;
		for (int idx=0;idx<numMassClusters;idx++)
		{
			Mat drw= Mat::zeros( input.size(), CV_8UC3 );
			ostringstream convert;
			convert << "cluster_"<<idx<<".jpg";	
			vector<vector<Point> > clusterContours;
			int npts[clusterContours.size()];
			for (int contourIdx=0;contourIdx<resContours.size();contourIdx++)
			{
				if (bestLabels.at<int>(contourIdx)==idx)
				{
					drawContours( drw, resContours, contourIdx, white, -1, 8, resHierarchy, 0, Point() );	
					
					clusterContours.push_back(resContours[contourIdx]);
				}
				
			}
			/*imshow(convert.str(), drw);*/
			double clusterRMeasure=calcRMeasure(clusterContours);
			/*cout<<convert.str()<<" r="<<clusterRMeasure<<endl;*/
			segments.push_back(make_pair(drw, clusterRMeasure));		
			//imwrite(convert.str(), drw);
		}
		sort(segments.begin(), segments.end(), sortSegment);

		int maxMerge=3;
		Mat segmentMerge=Mat::zeros( input.size(), CV_8UC3 );
		for (int idx=0;idx<maxMerge;idx++)
		{
			bitwise_or(segmentMerge, segments[idx].first, segmentMerge);
		}
		output=segmentMerge;

}

void segmentLettersBandwidth(Mat &input, Mat &output)
{
	Mat resGabor=Mat::zeros(input.size(), CV_8U);
	for (int wavelength=1;wavelength<=12;wavelength++)
	{
		Mat kernel;
		Mat dest;

		getLogGaborKernel(kernel,3,3,wavelength*10, 100.0, 10.0);
		
		filter2D(input.clone(), dest, CV_32F, kernel);
		
		ostringstream convert;
		convert << "length_"<<wavelength;	

		Mat planes[2]={Mat(dest.size(), CV_32F), Mat(dest.size(), CV_64F)};

		split(dest, planes);			

		Mat phase=Mat(input.size(), CV_8U);
		cv::phase(planes[0], planes[1], phase);

		Mat phaseConvert=Mat(phase.size(), CV_8U);
		double minVal, maxVal;
		minMaxLoc(phase, &minVal, &maxVal);
		phase.convertTo(phaseConvert, CV_8U, 255.0/(maxVal - minVal), -minVal * 255.0/(maxVal - minVal));
		cv::threshold(phaseConvert, phaseConvert, (minVal+maxVal)/2, 255, cv::THRESH_BINARY);
		bitwise_or(phaseConvert,resGabor,resGabor);

		/*imshow("gabor",resGabor);*/
	
	}

	output=resGabor;

}


	void edgeDetection(Mat &input, Mat &output) {
		/*ALOG("LINHA A");*/
		Mat invertedInput=Mat(input.size(), input.type());
		bitwise_not(input,invertedInput);
		
		Mat gray=Mat(input.size(), CV_8U);
	    cvtColor(input,gray,CV_BGR2GRAY);

	    Mat grayInRGB=Mat(input.size(), input.type());
		cvtColor(gray, grayInRGB, CV_GRAY2BGR);

	    Mat threshold_result;
	    double threshold=morphoThreshold(gray, threshold_result);

		Mat firstMask;
		Mat bigElement=getStructuringElement( MORPH_RECT,
	                                       Size( 6, 6 ));
		closing(threshold_result,firstMask, bigElement);
		/*imshow("firstMask", firstMask);*/
		Mat firstGrayMask=Mat(input.size(), input.type());		
		reconstructColorFromMask(grayInRGB,firstMask,firstGrayMask);
		//bitwise_not(reconstruct, reconstruct);		
		
		int numClusters=3;
		Mat clusters[numClusters];		
		runKmeansOnImage(invertedInput, numClusters, clusters);

		Mat secondMask;
		findFinalCluster(input,numClusters, clusters,secondMask);		
		//imshow("secondMask", secondMask);

		Mat invSecondMask;
		bitwise_not(secondMask,invSecondMask);
		//imshow("inverted", invSecondMask);

		Mat thirdMask=firstMask & secondMask;
		//imshow("thirdMask", thirdMask);		

		Mat secondGrayMask=Mat(input.size(), input.type());		
		reconstructColorFromMask(grayInRGB,secondMask,secondGrayMask);

		Mat invSecondGrayMask=Mat(input.size(), input.type());		
		reconstructColorFromMask(grayInRGB,invSecondMask,invSecondGrayMask);
		
		//bitwise_not(finalCluster,finalCluster);
		/*imshow("secondGrayMask",secondGrayMask);
		imshow("firstGrayMask", firstGrayMask);*/
		
		Mat firstSkeletonMask;		
		skeleton(firstMask, firstSkeletonMask);		
		/*imshow("firstskeleton", firstSkeletonMask);*/

		int firstMaskPixels=countNonZero(firstMask);
		int firstSkelPixels=countNonZero(firstSkeletonMask);
		double firstFreq=(double)firstMaskPixels/firstSkelPixels;
		//cout<<"firstMaskPixels="<<firstMaskPixels<<" - firstSkelPixels="<<firstSkelPixels<<" - Freq="<<firstFreq<<endl;

		Mat secondSkeletonMask;		
		skeleton(invSecondMask, secondSkeletonMask);		
		

		int secondMaskPixels=countNonZero(invSecondMask);
		int secondSkelPixels=countNonZero(secondSkeletonMask);
		double secondFreq=(double)secondMaskPixels/secondSkelPixels;
		//cout<<"secondMaskPixels="<<secondMaskPixels<<" - secondSkelPixels="<<secondSkelPixels<<" - Freq="<<secondFreq<<endl;
		/*imshow("invSecondGrayMask", invSecondGrayMask);
		imshow("invSecondMask", invSecondMask);
		imshow("skeleton", secondSkeletonMask);*/
		/*ALOG("LINHA B");*/
		/*Mat resGabor;
		int maxFilters=6;
		double minPhase=std::numeric_limits<double>::max();
		for (int wavelength=1;wavelength<=maxFilters;wavelength++)
		{
		
			Mat kernel;
			Mat dest;
			double waveL=secondFreq*wavelength;
			double sigma=waveL/maxFilters;
			getLogGaborKernel(kernel,15,15,waveL, sigma, 90.0);
			
			filter2D(invSecondGrayMask.clone(), dest, CV_32F, kernel);
		
			Mat planes[2]={Mat(dest.size(), CV_32F), Mat(dest.size(), CV_32F)};

			split(dest, planes);			

			Mat phase=Mat(secondGrayMask.size(), CV_8U);
			cv::phase(planes[0], planes[1], phase);
			phase=abs(phase);			
		
			double meanPhase=mean(phase)[0];
			Mat phaseConvert=Mat(phase.size(), CV_8U);
			double minVal, maxVal;
			minMaxLoc(phase, &minVal, &maxVal);
			phase.convertTo(phaseConvert, CV_8U);
			

			cv::threshold(phaseConvert, phaseConvert, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
			
			bitwise_not(phaseConvert,phaseConvert);
			
			if(meanPhase<=minPhase)
			{
				resGabor=phaseConvert & invSecondMask;
				minPhase=meanPhase;
			}
		
		}*/
		
		/*ALOG("FINAL");*/

		
		/*Mat outputConverted=Mat(invSecondMask.size(), input.type());
		cvtColor(invSecondMask, outputConverted, CV_GRAY2BGR);*/
		output=invSecondMask;

	}

void segmentROI(){
	/*		bitwise_and(segmentMergeRGB,finalCluster, segmentMergeRGB);
		vector<vector<Point> > segmentContours;
		vector<Vec4i> segmentHierarchy;
		cv::threshold(segmentMergeRGB, segmentMergeRGB, threshold, 255, cv::THRESH_BINARY);		
		findContours( segmentMergeRGB.clone(), segmentContours, segmentHierarchy ,CV_RETR_EXTERNAL, CV_CHAIN_APPROX_TC89_L1, Point(0, 0) );

		Rect finalRect;
		vector<Point> fullPoly;
		for(int idx=0;idx<segmentContours.size();idx++)
		{
			vector<Point> poly;
			approxPolyDP( Mat(segmentContours[idx]), poly, 3, true );
			fullPoly.insert(fullPoly.end(), poly.begin(), poly.end());
		}
		finalRect=minAreaRect( Mat(fullPoly) ).boundingRect();*/
		
		/*Mat roi=Mat(reconstruct & segmentMerge, finalRect);
		cvtColor(roi,roi,CV_8U);
		cv::threshold(roi, roi, threshold, 255, cv::THRESH_BINARY);*/	
}

	void drawContours(vector<vector<Point> > &contours, vector<Vec4i> hierarchy, Size size, Mat &drawing)
	{
		RNG rng(12345);
		drawing = Mat::zeros( size, CV_8UC3 );
  		for( int i = 0; i< contours.size(); i++ )
     	{
     		Mat dr=Mat::zeros( size, CV_8UC3 );
	       	Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
	    
	       	drawContours( dr, contours, i, color, 2, 8, hierarchy, 0, Point() );	    
     	}
	}

	void skeleton(Mat &input, Mat& output)
	{
		Mat skel(input.size(), CV_8U, Scalar(0));
		Mat temp(input.size(), CV_8U);
		Mat element2 = getStructuringElement(MORPH_CROSS, Size(3, 3));
		
		Mat clone=input.clone();
		bool done;
		int run=0;
		int maxrun=2;
		do
		{
		  morphologyEx(clone, temp, MORPH_OPEN, element2);		  
		  bitwise_not(temp, temp);		  
		  bitwise_and(clone, temp, temp);
		  bitwise_or(skel, temp, skel);		  
		  erode(clone, clone, element2);
		  
		  double max;
		  minMaxLoc(clone, 0, &max);
		  done = (max == 0);
		  run++;
		} while (!done);
		
		output=skel;
		
	}


	double calcRMeasure(vector<vector<Point> > &contours)
	{
		vector<double> areas;
		double mean=0.0;
		for (int comIdx=0;comIdx<contours.size();comIdx++)
		{
			vector<Point> approx;
			approxPolyDP(contours[comIdx], approx, 5, true);			
			double area=contourArea(approx);
			areas.push_back(area);
			mean += area; 
		}
		mean/=contours.size();

		double r_measure=0.0;
		for (int areaIdx=0;areaIdx<areas.size();areaIdx++)
		{
			r_measure+=abs(areas[areaIdx]-mean);
		}
		return r_measure;
	}

	void runKmeansOnImage(Mat &input, int numClusters, Mat clusters[])
	{
		for (int idx=0;idx<numClusters;idx++)
		{
			clusters[idx]=Mat::zeros(input.size(),CV_8U);
		}

		Mat features=Mat(input.rows*input.cols, 3, CV_8U);
		int offset=input.cols;
		
		for (int ridx=0;ridx<input.rows;ridx++)
		{
			for (int cidx=0;cidx<input.cols;cidx++)
			{

				int index=ridx*offset+cidx;
				
				Vec3b pixel=input.at<Vec3b>(ridx,cidx);

				features.at<uchar>(index,0)=(uchar)pixel.val[0];
				features.at<uchar>(index,1)=(uchar)pixel.val[1];
				features.at<uchar>(index,2)=(uchar)pixel.val[2];
				
			}
		}
		
		Mat doubleFeatures=Mat(features.size(), CV_32F);
		features.convertTo(doubleFeatures, CV_32F);	
		
		Mat bestLabels;
		TermCriteria criteria=TermCriteria(TermCriteria::EPS, 30, 0.2 );
		cv::kmeans(doubleFeatures, numClusters, bestLabels, criteria,1, KMEANS_PP_CENTERS);		

		
		for (int ridx=0;ridx<input.rows;ridx++)
		{
			for (int cidx=0;cidx<input.cols-1;cidx++)
			{
				int index=ridx*offset+cidx;
				int label=bestLabels.at<int>(index,0);		
				clusters[label].at<uchar>(ridx,cidx)=255;
			}
		}
	}

	void reconstructColorFromMask(Mat &input, Mat &mask, Mat &output)
	{	
		Mat supermask=Mat(input.size(), input.type());
		cvtColor(mask, supermask,CV_GRAY2BGR);
		output=input & supermask;
	}

	void applyThreshold(Mat &input, Mat &output, double threshold_val)
	{
		Mat threshold_result=Mat(input.size(), input.type());
		threshold(input, threshold_result, threshold_val, 255, cv::THRESH_BINARY);
		output=threshold_result;
	}

	void applyFilters(Mat &input, Mat &output)
	{	
		Mat kern1=Mat::zeros(3,3, CV_8S);
		kern1.at<uchar>(1,0)=-1;
		kern1.at<uchar>(1,2)=1;
		
		Mat filter1_result;
		filter2D(input, filter1_result, -1, kern1, Point(-1,-1), 0, BORDER_DEFAULT);	

		Mat kern2=kern1.t();	
		Mat filter2_result;
		filter2D(input, filter2_result, -1, kern2, Point(-1,-1), 0, BORDER_DEFAULT);

		Mat max_filter;
		Mat abs_filter1=abs(filter1_result);
		Mat abs_filter2=abs(filter2_result);
		filter1_result.release();
		filter2_result.release();
		max(abs_filter1, abs_filter2, max_filter);
		abs_filter1.release();
		abs_filter2.release();
		output=max_filter;
	}


	void gradient(Mat &input, Mat &output, Mat &element)
	{

		Mat erode_res;
		Mat dilate_res;
		erode(input, erode_res, element);
		dilate(input, dilate_res, element);
		Mat gradient_res;
		gradient_res=dilate_res - erode_res;
		
		erode_res.release();	
		dilate_res.release();

		output=gradient_res;
	}

	void openClose(Mat &input, Mat &output, Mat &element)
	{
		Mat open_result;
		opening(input,open_result, element);
		Mat close_result;
		closing(open_result, close_result, element);
		open_result.release();
		output=close_result;
	}

	void closeOpen(Mat &input, Mat &output, Mat &element)
	{
		Mat close_result;
		closing(input, close_result, element);
		Mat open_result;
		opening(close_result,open_result, element);	
		close_result.release();
		output=open_result;
	}

	void opening(Mat &input, Mat& output, Mat &element)
	{
		Mat erode_res;
		Mat dilate_res;
		
		erode( input, erode_res, element );
		dilate(erode_res, dilate_res, element);
		erode_res.release();
		output=dilate_res;
	}

	void closing(Mat &input, Mat& output, Mat &element)
	{
		Mat erode_res;
		Mat dilate_res;

		dilate(input, dilate_res, element);
		erode(dilate_res, erode_res, element );
		dilate_res.release();
		output=erode_res;
	}



	void componentLabeling(Mat &input, Vector<Point> groups)
	{

	}

	void closeholes(Mat &input, Mat &element, Mat &output)
	{
		Mat negImage=255-input;
		Mat infRecOutput;

		Mat frame=Mat(input.rows-20, input.cols-20, input.type());
		Scalar value = Scalar( 255, 255, 255 );
	  	copyMakeBorder( frame, frame, 10, 10, 10, 10, BORDER_CONSTANT, value );

		infreconstruction(frame, negImage, element, output);

	}


	void infreconstruction(Mat &input, Mat &cond, Mat &element, Mat &output)
	{
		int total=input.rows*input.cols;
		conditional_dilate(input, cond, element, total, output);
	}

	void conditional_dilate(Mat &input, Mat &cond, Mat &element, int n, Mat &output)
	{
		Mat intersect=min(input, cond);
		Mat tmp;
		for (int index=0;index<=n;index++)
		{
			Mat temp;
			intersect.copyTo(temp);
			dilate(intersect,tmp, element);
			intersect=min(tmp, cond);
			/*Mat compareRes;
			compare(intersect, temp, compareRes, CMP_EQ);*/

		}
		output=intersect;
	}

	void getLogGaborKernel(Mat &kernel, int rows, int cols, double f0, double sigmaOnf, double thetaZero)
	{
		//norient=(norient<1)? 1:norient;

		thetaZero=(thetaZero==0.0)? 0.1:thetaZero;
		Mat x=Mat(rows,cols,CV_32F);
		Mat y=Mat(rows,cols,CV_32F);
		
		meshgrid(Range(-cols/2,cols/2), Range(-rows/2,rows/2),x, y);
		x=x/cols;
		y=y/rows;
		
		Mat radius=Mat(rows,cols,CV_32F);
		Mat theta_mat=Mat(rows,cols,CV_32F);
		for (int ridx=0;ridx<theta_mat.rows;ridx++)
			for (int cidx=0;cidx<theta_mat.cols;cidx++)
			{
				theta_mat.at<float>(ridx,cidx)=atan2(y.at<float>(ridx,cidx),x.at<float>(ridx,cidx));
				radius.at<float>(ridx,cidx)=sqrt(pow(x.at<float>(ridx,cidx),2)+pow(y.at<float>(ridx,cidx),2));
			}
		radius.at<float>(rows/2,cols/2)=0.0;
		Mat sintheta=Mat(rows,cols,CV_32F);
		Mat costheta=Mat(rows,cols,CV_32F);

		for (int i=0;i<theta_mat.rows;i++)
			for (int j=0;j<theta_mat.cols;j++)
			{

				sintheta.at<float>(i,j)=sin(theta_mat.at<float>(i,j));
				costheta.at<float>(i,j)=cos(theta_mat.at<float>(i,j));
			}

		/*theta_mat.release();*/
		float wavelength;
		//float f0;
		//Component radial
		Mat radial=Mat(rows,cols,CV_32F);
		float angle;
		
		/*wavelength=minWaveLength*pow(mul,nscale);
		f0=1.0/wavelength;*/
		
		for (int ridx=0;ridx<theta_mat.rows;ridx++)
			for (int cidx=0;cidx<theta_mat.cols;cidx++)
			{
				float radval=radius.at<float>(ridx,cidx);
				radial.at<float>(ridx,cidx)=exp(-1*pow(log(radval/f0),2)/2.0*pow(log(sigmaOnf/f0),2));
			}

		radial.at<float>(rows/2,cols/2)=0.0;
		/*radius.release();*/
		//Component angular
		angle=thetaZero*M_PI/180;

		Mat ds=Mat(rows,cols,CV_32F);
		Mat dc=Mat(rows,cols,CV_32F);

		float cosangle=cos(angle);
		float sinangle=sin(angle);
		for (int ridx=0;ridx<theta_mat.rows;ridx++)
			for (int cidx=0;cidx<theta_mat.cols;cidx++)
			{
				ds.at<float>(ridx,cidx)=sintheta.at<float>(ridx,cidx)*cosangle-costheta.at<float>(ridx,cidx)*sinangle;
				dc.at<float>(ridx,cidx)=costheta.at<float>(ridx,cidx)*cosangle-sintheta.at<float>(ridx,cidx)*sinangle;
			}
/*		sintheta.release();
		costheta.release();*/
		Mat dtheta=Mat(rows,cols,CV_32F);
		float thetaSigma=M_PI/4*sqrt(2*log(2));

		for (int ridx=0;ridx<theta_mat.rows;ridx++)
			for (int cidx=0;cidx<theta_mat.cols;cidx++)
			{
				dtheta.at<float>(ridx,cidx)=std::min<double>(abs(atan2(ds.at<float>(ridx,cidx),dc.at<float>(ridx,cidx)))*90,M_PI);	
			}
/*		ds.release();
		dc.release();*/
		Mat angular=Mat(rows,cols,CV_32F);

		for (int ridx=0;ridx<theta_mat.rows;ridx++)
			for (int cidx=0;cidx<theta_mat.cols;cidx++)
			{			
				float thetaval=dtheta.at<float>(ridx,cidx);
				
				float temp=-pow(thetaval-thetaZero,2)/2*pow(thetaSigma,2);

				angular.at<float>(ridx,cidx)=exp(temp);
			}

		
		kernel=radial.mul(angular);		

/*		radial.release();
		angular.release();
		dtheta.release();*/
		

		
	}



	// baseado em http://answers.opencv.org/question/11788/is-there-a-meshgrid-function-in-opencv/
	void meshgrid(const Range &xgv, const Range &ygv, Mat &X, Mat &Y)
	{
	  std::vector<double> t_x, t_y;
	  for (int i = xgv.start; i <= xgv.end; i++) t_x.push_back(i);
	  for (int i = ygv.start; i <= ygv.end; i++) t_y.push_back(i);
	  Mat xgv_mat=Mat(t_x);
	  Mat ygv_mat=Mat(t_y);
	  repeat(xgv_mat.reshape(1,1), ygv_mat.total(), 1, X);
	  repeat(ygv_mat.reshape(1,1).t(), 1, xgv_mat.total(), Y);  
	}








/*double distance(Mat &vec1, Mat &vec2, Mat stddev)
{
	Mat rest=(vec1-vec2)/stddev;
	Mat dist;
	cv::pow(rest,2,dist);
	cv::reduce(dist,dist,1,CV_REDUCE_SUM);
	return sqrt(dist.at<double>(0,0));
}

void kmeansImage(Mat &features, int numClusters, int maxIter, double eps, Mat &output)
{	
	Mat featuresCast=Mat(features.size(), CV_64F);
	features.convertTo(featuresCast, CV_64F);
	cout<<"kmeans 1"<<endl;

	Mat stddev=Mat_<double>(1,features.cols);
	for (int clusterIdx=0;clusterIdx<numClusters;clusterIdx++)
	{
		Mat channelMean, channelStdDev;
		meanStdDev(features.col(clusterIdx),channelMean,channelStdDev);
		cout<<"kmeans 2"<<channelStdDev<<endl;
		
		stddev.at<double>(0,clusterIdx)=channelMean.at<double>(0,0);
	}
	
	
	Mat centroids[numClusters];
	for (int idx=0;idx<numClusters;idx++)
	{
		int matIdx=rand()%featuresCast.rows;
		centroids[idx]=featuresCast.row(matIdx);
	}
	cout<<"kmeans 3"<<endl;
	Mat labels=Mat_<int>(1,featuresCast.rows);
	double gen=0.0;

	Mat newCentroids[numClusters];
	for (int newIdx=0;newIdx<numClusters;newIdx++)
	{
		newCentroids[newIdx]=Mat::zeros(1,featuresCast.cols, CV_64F);
	}
	
	int clusterCounters[]={0,0,0};
	for (int iter=0;iter<maxIter; iter++)
	{
		cout<<"kmeans A iter "<<iter<<endl;
		for (int rowid=0;rowid<featuresCast.rows;rowid++)
		{
			Mat row=featuresCast.row(rowid);
			cout<<"kmeans B iter "<<iter<<" rowid="<<rowid<<endl;
			double minDist=std::numeric_limits<double>::max();
			int minClusterIdx=0;
			for (int clusterIdx=0;clusterIdx<numClusters;clusterIdx++)
			{
				cout<<"kmeans B iter "<<iter<<" clusterIdx="<<clusterIdx<<endl;
				double curDist=distance(row,centroids[clusterIdx],stddev);
				if (curDist<minDist)
				{
					minClusterIdx=clusterIdx;
					minDist=curDist;
				}

			}
			newCentroids[minClusterIdx]+=row;
			clusterCounters[minClusterIdx]++;

			labels.at<int>(0,rowid)=minClusterIdx;
		}


		for (int clusterIdx=0;clusterIdx<numClusters;clusterIdx++)
		{
			centroids[clusterIdx].release();
			centroids[clusterIdx]=newCentroids[clusterIdx]/clusterCounters[clusterIdx];
			newCentroids[clusterIdx]=Scalar(0.0);
			clusterCounters[clusterIdx]=0;
		}

	}

	output=labels;



}
*/