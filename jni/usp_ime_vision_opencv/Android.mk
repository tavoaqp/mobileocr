LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

include /home/gustavo/tools/OpenCV-2.4.5-android-sdk/sdk/native/jni/OpenCV.mk

LOCAL_MODULE    := pre_mobileocr
LOCAL_SRC_FILES += PreOcrMobile.cpp PreOcrMobileJNI.cpp
LOCAL_LDFLAGS += -llog -ldl

include $(BUILD_SHARED_LIBRARY)
