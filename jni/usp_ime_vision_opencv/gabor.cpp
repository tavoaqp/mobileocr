#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <math.h>
#include <stdio.h>
#include <iostream>

using namespace cv;
using namespace std;

Mat mkKernel(int ks, double sig, double th, double lm, double ps);
Mat getLogGaborKernel( Size ksize, double f, double f0, double sigmaf, double theta, double theta0, double sigma_theta);
void edgeDetection(Mat &input, Mat &output);

int kernel_size=41;
int pos_sigma= 5;
int pos_lm = 50;
int pos_th = 0;
int pos_psi = 90;
Mat src_f;
Mat dest;

void Process(int , void *)
{
    double sig = pos_sigma;
    double lm = 0.5+pos_lm/100.0;
    double th = pos_th;
    double ps = pos_psi;
    Mat kernel = mkKernel(kernel_size, sig, th, lm, ps);
    Mat kernel2 = 
    filter2D(src_f, dest, CV_32F, kernel);
    imshow("Process window", dest);
    Mat Lkernel(kernel_size*20, kernel_size*20, CV_32F);
    resize(kernel, Lkernel, Lkernel.size());
    Lkernel /= 2.;
    Lkernel += 0.5;
    imshow("Kernel", Lkernel);
    Mat mag;
    pow(dest, 2.0, mag);
    imshow("Mag", mag);
}

int main(int argc, char** argv)
{    
    // VideoCapture cap;
    // if (argc > 1) {
    //     cap = VideoCapture(""argv[1]"");
    // } else {
    //     cap = VideoCapture(0);
    // }

    // if (!cap.isOpened()) {
    //     return -1;
    // }
    Mat image=imread("../13.jpg",1);ri
    // cap >> image;
    Mat output;
    //Canny(image, src_f, 250,250);
    edgeDetection(image,src_f);

    Mat fImage;
    src_f.convertTo(fImage, CV_32F);


    cout << "Direct transform...\n";
    Mat fourierTransform;
    dft(fImage, fourierTransform, cv::DFT_SCALE|cv::DFT_COMPLEX_OUTPUT);



    cout << "Inverse transform...\n";
    Mat inverseTransform;
    dft(fourierTransform, inverseTransform, cv::DFT_INVERSE|cv::DFT_REAL_OUTPUT);

    // Back to 8-bits
    Mat finalImage;
    inverseTransform.convertTo(finalImage, CV_8U);


    //image=output;
    imshow("Output", finalImage);

    //imshow("Src", image);
    // Mat src;
    // cvtColor(image, src, CV_BGR2GRAY);
    // src.convertTo(src_f, CV_32F, 1.0/255, 0);
    if (!kernel_size%2)
    {
        kernel_size+=1;
    }
    namedWindow("Process window", 1);
    createTrackbar("Sigma", "Process window", &pos_sigma, kernel_size, Process);
    createTrackbar("Lambda", "Process window", &pos_lm, 100, Process);
    createTrackbar("Theta", "Process window", &pos_th, 180, Process);
    createTrackbar("Psi", "Process window", &pos_psi, 360, Process);
    Process(0,0);
    waitKey(0);
    return 0;
}
