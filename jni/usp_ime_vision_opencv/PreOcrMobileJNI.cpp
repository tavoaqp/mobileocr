#include <jni.h>
#include <android/log.h>
#include "opencv2/opencv.hpp"

using namespace cv;

void edgeDetection(Mat &input, Mat &output);

extern "C" JNIEXPORT void JNICALL Java_usp_ime_vision_opencv_OcrPreProcess_nativePreProcess(JNIEnv *, jobject, jlong input, jlong output) {
	

	Mat &img = *(Mat *) input;
	Mat &outputImg= *(Mat *) output;
	edgeDetection(img,outputImg);

}
