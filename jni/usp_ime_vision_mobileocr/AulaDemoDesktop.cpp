#include "opencv2/opencv.hpp"

using namespace cv;

void ColorSegment(Mat &img);

int main(int argc, char *argv[]) {
    Mat frame;
    VideoCapture cap;
    if (argc > 1) {
        cap = VideoCapture(argv[1]);
    } else {
        cap = VideoCapture(0);
    }
    
    if (!cap.isOpened()) {
        return -1;
    }
    
    namedWindow("Video", 1);
    while (1) {
        cap >> frame;
        ColorSegment(frame);
        imshow("Video", frame);
        waitKey(33);
        //if (waitKey(33) != 0) break;
    }
    
    return 0;
}




